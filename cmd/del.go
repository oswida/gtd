package cmd

import (
	"gtd/env"
	"strconv"

	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
)

func CommandDel(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "delete",
		Help:    "delete task",
		Aliases: []string{"d"},

		Args: func(a *grumble.Args) {
			a.String("taskId", "task identifier")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			arg := c.Args.String("taskId")
			confirm := false
			prompt := &survey.Confirm{
				Message: "Delete task " + arg + "?",
			}
			err := survey.AskOne(prompt, &confirm)
			if err != nil {
				return err
			}
			if confirm {
				id, err := strconv.Atoi(arg)
				if err != nil {
					return err
				}
				err = env.Todo.RemoveTask(id)
				if err != nil {
					return err
				}
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

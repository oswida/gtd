package cmd

import (
	"gtd/env"
	"strconv"

	"github.com/desertbit/grumble"
)

func CommandPrio(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "prio",
		Help:    "set task priority",
		Aliases: []string{"p"},

		Args: func(a *grumble.Args) {
			a.String("taskId", "task identifier")
			a.String("priority", "priority level (A,B,C,D)")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			arg := c.Args.String("taskId")
			prio := c.Args.String("priority")

			id, err := strconv.Atoi(arg)
			if err != nil {
				return err
			}
			err = env.Todo.SetPrio(id, prio)
			if err != nil {
				return err
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

package cmd

import (
	"gtd/env"
	"path/filepath"

	"github.com/desertbit/grumble"
)

func CommandStat(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "stat",
		Help:    "show task stats",
		Aliases: []string{"s"},

		Flags: func(f *grumble.Flags) {
			f.Bool("a", "all", false, "show stats for all task files from current directory")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			stats, err := env.Todo.Stats(c.Flags.Bool("all"))
			if err != nil {
				return err
			}
			title := env.Todo.CurrentFile
			if c.Flags.Bool("all") {
				title = filepath.Dir(env.Todo.CurrentFile)
			}
			env.Printer.Stats(title, c.Flags.Bool("table"), stats)
			return nil
		},
	}
}

package cmd

import (
	"gtd/env"

	"github.com/desertbit/grumble"
)

func CommandList(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "list",
		Help:    "list tasks",
		Aliases: []string{"l"},

		Flags: func(f *grumble.Flags) {
			f.String("f", "filter", "", "filter data, can be done/active/today/prj:<name>/ctx:<name>")
			f.String("s", "sort", "", "sort data, can be [prio/creo/prj/ctx/due][asc/desc]")
			f.Bool("a", "all", false, "show tasks from all files in working directory")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			env.Printer.List(c.Flags.Bool("table"),
				c.Flags.String("sort"),
				c.Flags.String("filter"),
				c.Flags.Bool("dates"),
				c.Flags.Bool("all"))
			return nil
		},
	}
}

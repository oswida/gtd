package cmd

import (
	"gtd/env"
	"strconv"

	"github.com/desertbit/grumble"
)

func CommandTimer(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "timer",
		Help:    "set timer for task",
		Aliases: []string{"t"},

		Args: func(a *grumble.Args) {
			a.String("taskId", "task identifier")
			a.String("command", "start|stop|remove")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			arg := c.Args.String("taskId")
			action := c.Args.String("command")
			id, err := strconv.Atoi(arg)
			if err != nil {
				return err
			}
			err = env.Todo.TimerAction(id, action)
			if err != nil {
				return err
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

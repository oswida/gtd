package cmd

import (
	"gtd/env"
	"strconv"

	"github.com/desertbit/grumble"
)

func CommandToggle(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "toggle",
		Help:    "toggle task status",
		Aliases: []string{"x"},

		Args: func(a *grumble.Args) {
			a.String("taskId", "task identifier")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			arg := c.Args.String("taskId")
			id, err := strconv.Atoi(arg)
			if err != nil {
				return err
			}
			err = env.Todo.ToggleTask(id)
			if err != nil {
				return err
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

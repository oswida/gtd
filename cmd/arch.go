package cmd

import (
	"gtd/env"
	"strconv"

	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
)

func CommandArchive(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "archive",
		Help:    "archive task",
		Aliases: []string{"z"},

		Args: func(a *grumble.Args) {
			a.String("taskIdOrStatus", "task identifier or 'done' for task status")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			arg := c.Args.String("taskIdOrStatus")
			confirm := false
			prompt := &survey.Confirm{
				Message: "Archive task " + arg + "?",
			}
			err := survey.AskOne(prompt, &confirm)
			if err != nil {
				return err
			}
			if confirm {
				if arg == "done" {
					err = env.Todo.ArchiveDone()
					if err != nil {
						return err
					}
				} else {
					id, err := strconv.Atoi(arg)
					if err != nil {
						return err
					}
					err = env.Todo.ArchiveTask(id)
					if err != nil {
						return err
					}
				}
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

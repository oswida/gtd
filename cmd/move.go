package cmd

import (
	"gtd/env"
	"strconv"

	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
)

func CommandMove(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "move",
		Help:    "move task to another file",
		Aliases: []string{"m"},

		Args: func(a *grumble.Args) {
			a.String("taskId", "task identifier")
			a.String("destFile", "destination file")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			arg := c.Args.String("taskId")
			outfile := c.Args.String("destFile")
			confirm := false
			prompt := &survey.Confirm{
				Message: "Move task " + arg + " to " + outfile + "?",
			}
			err := survey.AskOne(prompt, &confirm)
			if err != nil {
				return err
			}
			if confirm {
				id, err := strconv.Atoi(arg)
				if err != nil {
					return err
				}
				err = env.Todo.MoveTask(id, outfile)
				if err != nil {
					return err
				}
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

package cmd

import (
	"gtd/env"

	"github.com/desertbit/grumble"
)

func CommandAdd(env *env.RuntimeEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "add",
		Help:    "add new task",
		Aliases: []string{"a"},

		Args: func(a *grumble.Args) {
			a.String("todoLine", "todo data in double quotes")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineInput(c)
			err := env.Todo.AddTask(c.Args.String("todoLine"), true)
			if err != nil {
				return err
			}
			env.Printer.List(c.Flags.Bool("table"),
				"",
				"",
				c.Flags.Bool("dates"),
				false)
			return nil
		},
	}
}

module gtd

go 1.16

require (
	github.com/1set/todotxt v0.0.4
	github.com/AlecAivazis/survey/v2 v2.2.12 // indirect
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/c-bata/go-prompt v0.2.6 // indirect
	github.com/coreos/go-etcd v2.0.0+incompatible // indirect
	github.com/cpuguy83/go-md2man v1.0.10 // indirect
	github.com/desertbit/grumble v1.1.1
	github.com/devfacet/gocmd/v3 v3.1.2 // indirect
	github.com/inancgumus/screen v0.0.0-20190314163918-06e984b86ed3 // indirect
	github.com/pterm/pterm v0.12.18
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/spf13/viper v1.7.1
	github.com/ugorji/go/codec v0.0.0-20181204163529-d75b2dcb6bc8 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/sys v0.0.0-20210603125802-9665404d3644 // indirect
	golang.org/x/term v0.0.0-20210503060354-a79de5458b56 // indirect
)

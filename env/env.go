package env

import (
	"gtd/todo"
	"os"
	"path/filepath"

	"github.com/desertbit/grumble"
)

type RuntimeEnv struct {
	Todo    *todo.TodoService
	Printer *todo.TodoPrinter
	App     *grumble.App
}

func NewEnv() (*RuntimeEnv, error) {
	srv, err := todo.NewTodoService()
	if err != nil {
		panic(err)
	}
	printer := todo.NewTodoPrinter(srv)
	retv := &RuntimeEnv{
		Todo:    srv,
		Printer: printer,
	}
	app := grumble.New(&grumble.Config{
		Name:        "gtd",
		Description: "Console todo.txt application.",

		Flags: func(f *grumble.Flags) {
			f.Bool("t", "table", false, "print data in tabular form")
			f.Bool("d", "dates", false, "show create/completed dates")
			f.String("i", "input", "", "todo input file")
		},
	})
	retv.App = app
	return retv, nil
}

func (e *RuntimeEnv) DetermineInput(c *grumble.Context) {
	fl := c.Flags.String("input")
	_, err := os.Stat(fl)
	if fl != "" && err == nil {
		e.Todo.CurrentFile = fl
		e.Todo.CurrentDone = filepath.Join(filepath.Dir(fl), "done.txt")
	}
}

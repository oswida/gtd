package common

type StatType struct {
	Total     int
	Completed int
	Overdued  int
}

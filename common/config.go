package common

import (
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

func setDefaults(dir string) {
	viper.Set("tableprint", false)
}

func NewConfig() error {
	dir, _ := os.Getwd()
	viper.SetConfigName("gtd")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			setDefaults(dir)
			_, err = os.Create(filepath.Join(dir, "gtd.yaml"))
			if err != nil {
				return err
			}
			return viper.WriteConfig()
		}
	}
	return err
}

package todo

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	td "github.com/1set/todotxt"
	"github.com/pterm/pterm"
)

func MakeFileIfNotExists(fpath string, initial []byte) error {
	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		_, err = os.Create(fpath)
		if err != nil {
			return err
		}
		if len(initial) > 0 {
			err = ioutil.WriteFile(fpath, initial, os.ModePerm)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func FileList(fpath string) ([]string, error) {
	retv := []string{}
	lst, err := os.ReadDir(fpath)
	if err != nil {
		return nil, err
	}
	for _, entry := range lst {
		if !entry.IsDir() && !strings.HasPrefix(entry.Name(), ".") {
			retv = append(retv, entry.Name())
		}
	}
	return retv, nil
}

func TodoLine(task td.Task) []string {
	prio := ""
	switch task.Priority {
	case "A":
		prio = pterm.FgRed.Sprintf("A")
	case "B":
		prio = pterm.FgYellow.Sprintf("B")
	case "C":
		prio = pterm.FgMagenta.Sprintf("C")
	}
	cpl := ""
	txt := task.Todo
	if task.Completed {
		cpl = pterm.FgGreen.Sprintf("✓")
		txt = pterm.FgGray.Sprintf(txt)
		prio = pterm.FgGray.Sprintf(prio)
	}
	proj := ""
	if len(task.Projects) > 0 {
		proj = pterm.FgBlue.Sprintf("+%s", strings.Join(task.Projects, ","))
	}
	ctx := ""
	if len(task.Contexts) > 0 {
		ctx = pterm.FgMagenta.Sprintf("@%s", strings.Join(task.Contexts, ","))
	}
	due := ""
	if !task.DueDate.IsZero() {
		if !task.IsOverdue() {
			due = pterm.FgLightGreen.Sprintf("due: %s", task.DueDate.Format("2006-01-02"))
		} else {
			due = pterm.FgRed.Sprintf("due: %s", task.DueDate.Format("2006-01-02"))
		}
	}
	completed := ""
	if task.HasCompletedDate() {
		completed = pterm.FgYellow.Sprintf("%s", task.CompletedDate.Format("2006-01-02"))
	}
	created := ""
	if task.HasCreatedDate() {
		if task.Completed {
			created = pterm.FgGray.Sprintf("%s", task.CreatedDate.Format("2006-01-02"))
		} else {
			created = pterm.FgWhite.Sprintf("%s", task.CreatedDate.Format("2006-01-02"))
		}
	}
	timer := ""
	_, ok := task.AdditionalTags["tstart"]
	if ok {
		sec1, err := strconv.Atoi(task.AdditionalTags["tstart"])
		if err != nil {
			return []string{}
		}
		t := time.Unix(int64(sec1), 0)
		timer = fmt.Sprintf("%s %v", pterm.FgLightYellow.Sprintf("⏲"), t.Format("15:04"))
	}
	if task.AdditionalTags["tstop"] != "" {
		sec1, err := strconv.Atoi(task.AdditionalTags["tstart"])
		if err != nil {
			return []string{}
		}
		sec2, err := strconv.Atoi(task.AdditionalTags["tstop"])
		if err != nil {
			return []string{}
		}
		t1 := time.Unix(int64(sec1), 0)
		t2 := time.Unix(int64(sec2), 0)
		duration := t2.Sub(t1)
		timer = fmt.Sprintf("%s %s-%s (%v min)", pterm.FgLightGreen.Sprintf("⏲"), t1.Format("15:04"), t2.Format("15:04"), duration.Round(time.Minute).Minutes())
	}
	return []string{fmt.Sprintf("%v.", task.ID), cpl, prio, created, completed, txt, proj, ctx, due, timer}
}

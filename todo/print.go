package todo

import (
	"fmt"
	"gtd/common"
	"path/filepath"
	"strings"
	"time"

	td "github.com/1set/todotxt"
	"github.com/pterm/pterm"
)

type TodoPrinter struct {
	ts *TodoService
}

func NewTodoPrinter(srv *TodoService) *TodoPrinter {
	return &TodoPrinter{
		ts: srv,
	}
}

func (p *TodoPrinter) List(tableprint bool, sort string, filter string, dates bool, all bool) {
	fmt.Println()
	var lst *td.TaskList
	if all {
		pterm.Info.Println(fmt.Sprintf("All tasks [%s]", filepath.Dir(p.ts.CurrentFile)))
		lst, _ = p.ts.TaskListAll()
	} else {
		pterm.Info.Println(fmt.Sprintf("Task list [%s]", time.Now().Format("2006-01-02")))
		lst, _ = p.ts.TaskList()
	}
	tasks := *lst
	if filter != "" {
		switch filter {
		case "done":
			tasks = tasks.Filter(td.FilterCompleted)
		case "active":
			tasks = tasks.Filter(td.FilterNotCompleted)
		case "today":
			tasks = tasks.Filter(td.FilterDueToday)
		default:
			if strings.HasPrefix(filter, "prj:") {
				tasks = tasks.Filter(td.FilterByProject(strings.Replace(filter, "prj:", "", 1)))
			} else if strings.HasPrefix(filter, "ctx:") {
				tasks = tasks.Filter(td.FilterByContext(strings.Replace(filter, "ctx:", "", 1)))
			}
		}
	}
	if sort != "" {
		switch sort {
		case "prioasc":
			_ = tasks.Sort(td.SortPriorityAsc)
		case "priodesc":
			_ = tasks.Sort(td.SortPriorityDesc)
		case "prjdesc":
			_ = tasks.Sort(td.SortProjectDesc)
		case "prjasc":
			_ = tasks.Sort(td.SortProjectAsc)
		case "ctxdesc":
			_ = tasks.Sort(td.SortContextDesc)
		case "ctxasc":
			_ = tasks.Sort(td.SortContextAsc)
		case "creodesc":
			_ = tasks.Sort(td.SortCreatedDateDesc)
		case "creoasc":
			_ = tasks.Sort(td.SortCreatedDateAsc)
		case "dueasc":
			_ = tasks.Sort(td.SortDueDateAsc)
		case "duedesc":
			_ = tasks.Sort(td.SortDueDateDesc)
		}
	}
	if tableprint {
		headers := []string{}
		if dates {
			headers = append(headers, "ID", " ", "Prio", "Created", "Completed", "Subject", "Prj", "Ctx", "Tags")
		} else {
			headers = append(headers, "ID", " ", "Prio", "Subject", "Prj", "Ctx", "Tags")
		}
		data := pterm.TableData{headers}
		for _, l := range tasks {
			line := TodoLine(l)
			if dates {
				data = append(data, line)
			} else {
				ln := line[:3]
				ln = append(ln, line[5:]...)
				data = append(data, ln)
			}
		}
		_ = pterm.DefaultTable.WithHasHeader().WithData(data).Render()
	} else {
		for _, l := range tasks {
			line := TodoLine(l)
			if all {
				line[0] = "•"
			}
			if dates {
				fmt.Println(strings.Join(line, " "))
			} else {
				fmt.Printf("%s %s\n", strings.Join(line[:3], " "), strings.Join(line[5:], " "))
			}
		}
	}
	fmt.Println()
}

func (p *TodoPrinter) Stats(fname string, tableprint bool, data common.StatType) {
	fmt.Println()
	pterm.Info.Println("Task statistics for:\n" + fname)
	fmt.Println()
	fmt.Printf(" Total tasks:\t%s\n", pterm.FgLightGreen.Sprint(data.Total))
	if data.Completed > 0 {
		fmt.Printf(" Completed:\t%s\n", pterm.FgLightGreen.Sprint(data.Completed))
	}
	if data.Overdued > 0 {
		fmt.Printf(" Overdued:\t%s\n", pterm.FgLightGreen.Sprint(data.Overdued))
	}
	fmt.Println()
}

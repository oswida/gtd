package todo

import (
	"errors"
	"fmt"
	"gtd/common"
	"os"
	"path/filepath"
	"strings"
	"time"

	td "github.com/1set/todotxt"
)

type TodoService struct {
	CurrentFile string
	CurrentDone string
}

func NewTodoService() (*TodoService, error) {
	retv := &TodoService{}

	dir, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	now := time.Now()
	fname := fmt.Sprintf("%s.todo.txt", now.Format("2006-01-02"))
	dname := "done.txt"
	retv.CurrentFile = filepath.Join(dir, fname)
	retv.CurrentDone = filepath.Join(dir, dname)

	err = MakeFileIfNotExists(retv.CurrentFile, []byte(""))
	if err != nil {
		return nil, err
	}
	err = MakeFileIfNotExists(retv.CurrentDone, []byte(""))
	if err != nil {
		return nil, err
	}
	return retv, nil
}

func (s *TodoService) TaskList() (*td.TaskList, error) {
	tasklist, err := td.LoadFromPath(s.CurrentFile)
	if err != nil {
		return nil, err
	}
	return &tasklist, nil
}

func (s *TodoService) TaskListAll() (*td.TaskList, error) {
	parent := filepath.Dir(s.CurrentFile)
	files, err := FileList(parent)
	if err != nil {
		return nil, err
	}
	tasklist := td.NewTaskList()
	for _, f := range files {
		if strings.HasSuffix(f, ".todo.txt") {
			lst, err := td.LoadFromPath(f)
			if err != nil {
				return nil, err
			}
			for _, t := range lst {
				tasklist.AddTask(&t)
			}
		}
	}
	return &tasklist, nil
}

func (s *TodoService) TaskListDir(fpath string) (*td.TaskList, error) {
	files, err := FileList(fpath)
	if err != nil {
		return nil, err
	}
	var tasklist td.TaskList = td.NewTaskList()
	for _, f := range files {
		if strings.HasSuffix(f, "todo.txt") {
			tl, err := td.LoadFromPath(f)
			if err != nil {
				return nil, err
			}
			for _, t := range tl {
				tasklist.AddTask(&t)
			}
		}
	}
	return &tasklist, nil
}

func (s *TodoService) FindTask(id int) (*td.Task, error) {
	tl, err := s.TaskList()
	if err != nil {
		return nil, err
	}
	t, err := tl.GetTask(id)
	return t, err
}

func (s *TodoService) ToggleTask(id int) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	t, err := tl.GetTask(id)
	if err != nil {
		return err
	}
	if !t.IsCompleted() {
		t.Complete()
	} else {
		t.Reopen()
	}
	return tl.WriteToPath(s.CurrentFile)
}

func (s *TodoService) ArchiveTask(id int) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	archive, _ := td.LoadFromPath(s.CurrentDone)
	t, err := tl.GetTask(id)
	if err != nil {
		return err
	}
	archive.AddTask(t)
	err = archive.WriteToPath(s.CurrentDone)
	if err != nil {
		return err
	}
	err = tl.RemoveTask(*t)
	if err != nil {
		return err
	}
	err = tl.WriteToPath(s.CurrentFile)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTask(id int) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	t, err := tl.GetTask(id)
	if err != nil {
		return err
	}
	err = tl.RemoveTask(*t)
	if err != nil {
		return err
	}
	return tl.WriteToPath(s.CurrentFile)
}

func (s *TodoService) AddTask(line string, autocreo bool) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	t, err := td.ParseTask(line)
	if err != nil {
		return err
	}
	if autocreo {
		t.CreatedDate = time.Now()
	}
	tl.AddTask(t)
	return tl.WriteToPath(s.CurrentFile)
}

func (s *TodoService) TimerAction(id int, action string) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	t, err := tl.GetTask(id)
	if err != nil {
		return err
	}
	if t.AdditionalTags == nil {
		t.AdditionalTags = map[string]string{}
	}
	switch action {
	case "start":
		t.AdditionalTags["tstart"] = fmt.Sprintf("%v", time.Now().Unix())
	case "stop":
		if t.AdditionalTags["tstart"] != "" {
			t.AdditionalTags["tstop"] = fmt.Sprintf("%v", time.Now().Unix()) //Format("2006-01-02_15-04")
		} else {
			return errors.New("Timer not started.")
		}
	case "remove":
		delete(t.AdditionalTags, "tstart")
		delete(t.AdditionalTags, "tstop")
	}
	return tl.WriteToPath(s.CurrentFile)
}

func (s *TodoService) ArchiveDone() error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	archive, _ := td.LoadFromPath(s.CurrentDone)
	for _, t := range *tl {
		archive.AddTask(&t)
	}
	err = archive.WriteToPath(s.CurrentDone)
	if err != nil {
		return err
	}
	for _, t := range *tl {
		err = tl.RemoveTask(t)
		if err != nil {
			return err
		}
	}
	err = tl.WriteToPath(s.CurrentFile)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) SetPrio(id int, prio string) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	t, err := tl.GetTask(id)
	if err != nil {
		return err
	}
	t.Priority = prio
	return tl.WriteToPath(s.CurrentFile)
}

func (s *TodoService) MoveTask(id int, fpath string) error {
	tl, err := s.TaskList()
	if err != nil {
		return err
	}
	err = MakeFileIfNotExists(fpath, []byte(""))
	if err != nil {
		return err
	}
	archive, _ := td.LoadFromPath(fpath)
	t, err := tl.GetTask(id)
	if err != nil {
		return err
	}
	archive.AddTask(t)
	err = archive.WriteToPath(fpath)
	if err != nil {
		return err
	}
	err = tl.RemoveTask(*t)
	if err != nil {
		return err
	}
	err = tl.WriteToPath(s.CurrentFile)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) Stats(all bool) (common.StatType, error) {
	retv := common.StatType{
		Completed: 0,
		Overdued:  0,
	}
	var tl *td.TaskList
	var err error
	if all {
		fl := filepath.Dir(s.CurrentFile)
		tl, err = s.TaskListDir(fl)
	} else {
		tl, err = s.TaskList()
	}
	if err != nil {
		return retv, err
	}
	retv.Total = len(*tl)
	for _, t := range *tl {
		if t.IsCompleted() {
			retv.Completed++
		}
		if t.IsOverdue() {
			retv.Overdued++
		}
	}
	return retv, nil
}

package main

import (
	"gtd/cmd"
	"gtd/env"
	"os"

	"github.com/pterm/pterm"
)

func main() {
	env, err := env.NewEnv()
	if err != nil {
		pterm.Error.WithShowLineNumber(false).Println(err)
		os.Exit(1)
	}
	registerCommands(env)

	err = env.App.Run()
	if err != nil {
		pterm.Error.WithShowLineNumber(false).Println(err)
		os.Exit(1)
	}
}

func registerCommands(env *env.RuntimeEnv) {
	env.App.AddCommand(cmd.CommandList(env))
	env.App.AddCommand(cmd.CommandAdd(env))
	env.App.AddCommand(cmd.CommandArchive(env))
	env.App.AddCommand(cmd.CommandDel(env))
	env.App.AddCommand(cmd.CommandMove(env))
	env.App.AddCommand(cmd.CommandPrio(env))
	env.App.AddCommand(cmd.CommandToggle(env))
	env.App.AddCommand(cmd.CommandTimer(env))
	env.App.AddCommand(cmd.CommandStat(env))
}
